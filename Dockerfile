FROM node:20.11.0-alpine3.19 as node
WORKDIR /app
COPY / ./
RUN npm install
RUN npm run build --prod
#stage 2
FROM nginx:alpine as dist
COPY --from=node /app/dist/adel /usr/share/nginx/html
COPY --from=node /app/nginx.conf /etc/nginx/conf.d/default.conf
